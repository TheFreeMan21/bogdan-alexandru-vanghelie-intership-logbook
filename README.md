**10.01**
- Morning meeting.
- Presentation of Ebits.dk . 
- Deciding the platform for comunication.-Discord
- Explaining Trello.
- Start making on product descriptions.
- Using Google Drive for sharing documents. 

**11.01**
- Morning meeting.
- Making on new product descriptions.
- Learning more about the eBits.

**12.01**
- Morning meeting.
- Brainstorming for a new project.
- Making on new product descriptions.

**13.01**
- Morning meeting.
- Brainstorming for new project.
- Making on new product descriptions.


**14.01**
- Morning meeting.
- Making on new product descriptions.
- Taking an idea and thinking how to implement it.
- Reshearching a bit what components I need.

> 

**17.01**
- Morning meeting.
- Found the idea for the project the use case scenario. 
- Deciding the components needed for the project
- Telling and making order for the needed parts from Nikolaj. 


**18.01**
- Morning meeting.
- Making for product descriptions until the parts from Nikolaj get arrive.
- Researching the parts selected for the project. 

**19.01**
- Morning meeting.
- Making for product descriptions until the parts from Nikolaj get arrive.
- Researching the parts selected for the project. 
- Searching for new products for Ebits.

**20.01**
- Morning meeting.
- Working on web-site until the parts get delivered.
- Make research about the parts. 
- Search new interesting electronic parts for eBits.
 

**21.01**
- Morning meeting.
- I get the ordered parts for the projects. 
- Start working on the project.
> 


**24.01**
- Morning meeting.
- Testing each part if they work. 
 

**25.01**
- Morning meeting.
- Comparing the sensors that could be used for distance measuring.
 

**26.01**
- Morning meeting.
- Preparing the code for the "device" and testing it.
 

**27.01**
- Morning meeting.
- Debugging the errors from the code and testing it more.
- After testing in a closed enviorment I can start calibrating the Ultra sonic sensor for an outdoor test.

**28.01**
- Morning meeting.
- Did the outdoor test and 

**31.01**
- Morning meeting.
- Planing the 3D printed case.


**01.02**
- Morning meeting.
- Learning on how to use Fusion 360.


**02.02**
- Morning meeting.
- Starting the case.


**03.02**
- Morning meeting.
- Putting the case to be printed.


**04.02**
- Morning meeting. 
- Taking the case and inspecting it for errors.
> 


**07.02**
- Morning meeting.
- Working on second version of the Case and starting the article for the project

**08.02**
- Morning meeting.
- Putting the second version to be printed.
- Finding and correcting some more errors in the second revision.
- Finnishing the article 

**09.02**
- Morning meeting.
- Putting the third revision to be printed and taking the second one


**10.02**
- Morning meeting.
- Making the Final Test with the Final MVP
 

**11.02**
- Morning meeting. 
- Fixing some mistakes at the Article

**14.02**
- Morning meeting. 
- Making the Video of the MVP

**15.02**
- Morning meeting. 
- Reflecting Back at what I did in the Project

**16.02**
- Morning meeting. 
- Fixing some more things at the Video

**17.02**
- Morning meeting. 
- Redoing the Video
- Participating at the 3D Modelling course done by Nikolaj

**18.02**
- Morning meeting. 
- Updating Product Prices on the Store
- Doing Gifs for a product

**21.02**
- Morning meeting. 
- Choosing what Products Need Gifs

**22.02**
- Morning meeting. 
- Making a new Product Description and Putting the Product on the site
- Helping an Intership Colleague
- Publishing the Article

**23.02**
- Morning meeting. 
- Making more Gifs for Products

**24.02**
- Morning meeting. 
- Making more Gifs for Products
- Nikolaj visited us to bring the RFcat boards and showed us how to assemble them.

**25.02**
- Morning meeting. 
- Making more Gifs for Products

**28.02**
- Morning meeting. 
- Making more Gifs for Products

**01.03**
- Morning meeting. 
- Making more Gifs for Products

**02.03**
- Morning meeting. 
- Making more Gifs for Products

**03.03**
- Morning meeting. 
- Making more Gifs for Products

**04.03**
- Morning meeting. 
- Making more Gifs for Products

**07.03**
- Morning meeting. 
- Making more Gifs for Products

**08.03**
- Morning meeting. 
- Uploading the last Gifs

**09.03**
- Morning meeting. 
- Starting a project for a flutter App for Android and IOS
- Learning about Flutter

**10.03**
- Morning meeting. 
- Learning about Flutter

**11.03**
- Morning meeting. 
- Learning about Flutter

**14.03**
- Morning meeting. 
- Learning about Flutter

**15.03**
- Morning meeting. 
- Starting developement on the app

**16.03**
- Morning meeting. 
- Continuing working on the App

**17.03**
- Morning meeting. 
- Making a simple design for the App

**18.03**
- Morning meeting. 
- Working more on the App

**21.03**
- Morning meeting. 
- Connecting the App to Gabriel's Database and pulling and editing data from it.

**22.03**
-  Morning meeting. 
-  Working more on the App

**23.03**
- Morning meeting 
- Working more on the App, adding new features

**24.03**
- Morning meeting 
- Working more on the App, bug fixing

**25.03**
- Morning meeting 
- Working more on the App, adding new features

**28.03**
- Morning meeting 
- Working more on the App, adding new features

**29.03**
- Morning meeting 
- Working more on the App, adding new features

**30.03**
- Morning meeting 
- Working more on the App, bug fixing

**31.03**
- Morning meeting 
- Going to the eBits.dk HQ to build up some kits




